package com.tw.matrix;

import java.util.Scanner;

public class UpperTriangularMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of rows/columns:");
        int size = scanner.nextInt();

        int[][] matrix = new int[size][size];
        System.out.println("enter elements");
        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                matrix[row][column] = scanner.nextInt();
            }
        }

        String result=verifyUpperTriangularMatrix(size, matrix);
        System.out.println("Upper Triangular Matrix: "+result);

    }

    private static String verifyUpperTriangularMatrix(int size, int[][] matrix) {
        int flag = 0;
        for (int row = 1; row < size; row++) {
            for (int column = 0; column < row; column++) {
                if (matrix[row][column] != 0) {
                    flag = 1;
                    break;
                }
            }
        }

        if (flag == 1)
            return "No";
        else
            return "yes";
    }
}
